FROM openjdk:17
COPY target/hello-world-0.1.0.jar .
CMD [ "java", "-jar",  "hello-world-0.1.0.jar" ]
